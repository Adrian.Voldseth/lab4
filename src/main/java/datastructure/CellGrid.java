package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int rows;
	private int cols;
	private CellState[][] State;
	private CellState initialState;

    public CellGrid(int rows, int columns, CellState initialState) {
    		this.rows = rows;
    		this.cols = columns;
    		this.initialState = initialState;
    		this.State = new CellState[rows][columns];
    		
 		// TODO Auto-generated constructor stub
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
    	
    	if (row >= 0 && row <= numRows()) {
    		this.State[row][column] = element;
    		
    	}	
    	else {
    		throw new IndexOutOfBoundsException();
    	}
    }

    @Override
    public CellState get(int row, int column) {

        return this.State[row][column];
    }

    @Override
    public IGrid copy() {
    	IGrid Copy = new CellGrid(this.numRows(), this.numColumns(), initialState);
    	
    	for (int i = 0; i < this.numColumns(); i++) { 
    		for (int j = 0; j < this.numRows(); j++) { 
    			Copy.set(j, i, this.get(j, i));
    			} 
    		}
    	return Copy;
    }

 }
    
