package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	
	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				int rand = random.nextInt(3);
				if(rand == 0) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} 
				if(rand == 1) {
					currentGeneration.set(row, col, CellState.DEAD);
				}
				if(rand == 2) {
					currentGeneration.set(row, col, CellState.DYING);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
    	for (int i = 0; i < nextGeneration.numColumns(); i++) { 
    		for (int j = 0; j < nextGeneration.numRows(); j++) {
    			nextGeneration.set(j, i, getNextCell(j,i));
    			}
    		}
    	currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		if(this.getCellState(row, col) == CellState.ALIVE) {
			return CellState.DYING;
		}
		if(this.getCellState(row, col) == CellState.DYING) {
			return CellState.DEAD;
		}
		if(this.countNeighbors(row, col, CellState.ALIVE) == 2 & this.getCellState(row, col) == CellState.DEAD) {
			return CellState.ALIVE;
		} else {
			return CellState.DEAD;
		}

	}
	

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {

		int counter = 0;
		int[] rowP = new int[] {0,1,1,1,0,-1,-1,-1};
		int[] colP = new int[] {1,1,0,-1,-1,-1,0,1};
    	
    	for (int i = 0; i < rowP.length; i++) { 
    		try {
				if(this.getCellState(row + rowP[i], col + colP[i]) == state) {
					counter++;
				}else {
					continue;
				}
    		} catch(ArrayIndexOutOfBoundsException exception) {
    		    continue;
    		}
	}
    	return counter;
	}
	


	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}

}
